import os
import urllib.request as downloader
import sys

CPE_DICT_URL = 'https://nvd.nist.gov/feeds/xml/cpe/dictionary/official-cpe-dictionary_v2.3.xml.zip'
CPE_DICT_DOWNLOAD_DIR = 'cpe_dict'
CPE_DICT_FILE = 'official-cpe-dictionary_v2.3.xml.zip'
CPE_DICT_FILE_PATH = os.path.join(CPE_DICT_DOWNLOAD_DIR, CPE_DICT_FILE)


def download_dict():
    create_download_dir()
    download()


def create_download_dir():
    if not os.path.isdir(CPE_DICT_DOWNLOAD_DIR):
        os.mkdir(CPE_DICT_DOWNLOAD_DIR)


def download():
    if not os.path.isfile(CPE_DICT_FILE_PATH):
        print("Downloading CPE-Dictionary " + CPE_DICT_FILE)
        downloader.urlretrieve(CPE_DICT_URL, CPE_DICT_FILE_PATH)


def main():
    download_dict()


if __name__ == '__main__':
    sys.exit(main())

