# [CPE Dictionary](https://nvd.nist.gov/products/cpe) and [CVE Feeds](https://nvd.nist.gov/vuln/data-feeds) Analyzer

This Python tool performs the following tasks:

1. It searches for CVE entries in the CPE-Feeds without CPEs.
2. It searches for deprecated CPEs in the CPE-Dictionary.
3. It searches for CPEs that are in the CVE-Fees but not in the CPE-Dictionary. 

# Usage

To run the analyzer, go to the root directory (`cd` cpe_cve_analyzer) and execute the following command line:

```python
python analyzer.py
```

The results are saved in the `results` directory. It contains three text files:

* **cve_without_cpe_entries.txt** contains a list of CVE IDs, which does not have CPEs.  
	
* **deprecated_cpe.txt** contains a list of deprecated CPEs. 
	
* **synchronization_cve_cpe.txt** contains a list of CPEs that are found in the [CVE Feeds](https://nvd.nist.gov/vuln/data-feeds) but not in the [CPE Dictionary](https://nvd.nist.gov/products/cpe).


To avoid downloading the CPE Dictionary and [CVE Feeds](https://nvd.nist.gov/vuln/data-feeds) every time the analysis is run, a copy of the [CPE Dictionary](https://nvd.nist.gov/products/cpe) and CVE Feeds are stored locally. However, if you want to download the dictionary and the feeds again, you have to run the analysis with the parameter `download`:

```python
python analyzer.py download
```
 