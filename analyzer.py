import sys
import os
import cve_reader
import cve_downloader
import cpe_dowloader
import shutil
import cpe_reader
from cpe_dowloader import CPE_DICT_DOWNLOAD_DIR
from cve_downloader import CVE_FEEDS_DOWNLOAD_DIR
from cve_reader import XML_KEY

RESULTS_DIR = os.path.join(os.path.curdir, 'results')
RESULT_1_FILE_PATH = os.path.join(RESULTS_DIR, 'cve_without_cpe_entries.txt')
RESULT_2_FILE_PATH = os.path.join(RESULTS_DIR, 'deprecated_cpe.txt')
RESULT_3_FILE_PATH = os.path.join(RESULTS_DIR, 'synchronization_cve_cpe.txt')


def main():
    setup()
    analyze()


def setup():
    remove_downloads()
    cve_downloader.download_feeds()
    cpe_dowloader.download_dict()
    create_result_dir()


def remove_downloads():
    if "download" in sys.argv:
        if os.path.exists(CPE_DICT_DOWNLOAD_DIR):
            shutil.rmtree(CPE_DICT_DOWNLOAD_DIR)
        if os.path.exists(CVE_FEEDS_DOWNLOAD_DIR):
            shutil.rmtree(CVE_FEEDS_DOWNLOAD_DIR)


def create_result_dir():
    if os.path.exists(RESULTS_DIR):
        shutil.rmtree(RESULTS_DIR)
    os.mkdir(RESULTS_DIR)


def analyze():
    print("Running analysis....")
    analyze_cve_entries_without_cpe_entries()
    analyze_deprecated_cpe_entries()
    analyze_synchronization()
    print("Analysis finished. See results directory.")


def analyze_cve_entries_without_cpe_entries():
    result = []
    result_file = open(RESULT_1_FILE_PATH, 'w+')
    for feed in cve_reader.read_cve_feeds():
        for cve_entry in feed.get(XML_KEY):
            if not cve_reader.is_cve_entry_rejected(cve_entry) and not cve_reader.contains_cpe_entries(cve_entry):
                cve_id = get_cve_id(cve_entry)
                if not result.__contains__(cve_id):
                    result.append(cve_id)
                    result_file.write(cve_id + "\n")
    result_file.close()


def analyze_deprecated_cpe_entries():
    cpe_entries = cpe_reader.read_cpe_entries()
    deprecated_cpe_entries = cpe_reader.get_deprecated_cpe_entries(cpe_entries)
    result_file = open(RESULT_2_FILE_PATH, 'w+')
    for cpe in deprecated_cpe_entries:
        result_file.write(str(cpe) + "\n")
    result_file.close()


def analyze_synchronization():
    cpe_entries_in_cve_entries_set = get_cpe_entries_in_cve_entries_set()
    cpe_entries_names_set = set(cpe_reader.get_cpe_entries_names(cpe_reader.read_cpe_entries()))
    cpe_entries_not_in_cpe_dic = cpe_entries_in_cve_entries_set.difference(cpe_entries_names_set)
    result_file = open(RESULT_3_FILE_PATH, 'w+')
    for cpe in cpe_entries_not_in_cpe_dic:
        result_file.write(cpe + "\n")
    result_file.close()


def get_cpe_entries_in_cve_entries_set():
    cpe_entries_in_cve_entries = []
    for feed in cve_reader.read_cve_feeds():
        for cve_entry in feed.get(XML_KEY):
            cpe_entries_in_cve_entries.extend(cve_reader.get_cpe_entries_from_cve_entry(cve_entry))
    return set(cpe_entries_in_cve_entries)


def get_cve_id(cve_entry):
    return cve_entry.attrib.get('id')


if __name__ == '__main__':
    sys.exit(main())
